import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Header from "./Components/Header";
import TravelHistory from "./Screens/TravelHistory";
import AcuteRespiratoryIllness from "./Screens/AcuteRespiratoryIllness";
import TravelerOrPatient from "./Screens/TravelerOrPatient";
import Symptoms from "./Screens/Symptoms";
import HistoryOfExposure from "./Screens/HistoryOfExposure";
import NotPuiOrPum from "./Screens/NotPuiOrPum";
import PUM from "./Screens/PUM";
import PUI from "./Screens/PUI";
import TC from "./Screens/TC";
import AC from "./Screens/AC";
import DS from "./Screens/DS";
import "./App.css";
import NotPuiOrPumPatient from "./Screens/NotPuiOrPumPatient";
import PUMPatient from "./Screens/PUMPatient";
import PUIPatient from "./Screens/PUIPatient";

function App() {
  const travelHistory =
    "In the past 14 days to countries with local transmission and risk of importation";
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/traveler/travelhistory">
          <TravelHistory travelHistory={travelHistory} />
        </Route>
        <Route path="/traveler/symptoms">
          <Symptoms />
        </Route>
        <Route path="/traveler/historyofexposure">
          <HistoryOfExposure />
        </Route>
        <Route path="/patient/notpuiorpumpatient">
          <NotPuiOrPumPatient />
        </Route>
        <Route path="/traveler/notpuiorpum">
          <NotPuiOrPum />
        </Route>
        <Route path="/traveler/pum">
          <PUM />
        </Route>
        <Route path="/traveler/pui">
          <PUI />
        </Route>
        <Route path="/patient/acuterespiratoryillness">
          <AcuteRespiratoryIllness />
        </Route>
        <Route path="/patient/tc">
          <TC />
        </Route>
        <Route path="/patient/ac">
          <AC />
        </Route>
        <Route path="/patient/ds">
          <DS />
        </Route>
        <Route path="/patient/pumpatient">
          <PUMPatient />
        </Route>
        <Route path="/patient/puipatient">
          <PUIPatient />
        </Route>
        <Route path="/">
          <TravelerOrPatient />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
