import React from "react";
import Paragraph from "../Components/Paragraph";
import { Link } from "react-router-dom";

const AC = () => {
  const A =
    "A. A patient with severe acure respiratory infection or a typical pneumonia AND requiring hospitalization AND with no other etiology to fully explain the clinical presentation, regardless of exposure history.";
  const B = "B. Cluster of ILI cases in household or workplace. ";

  return (
    <div>
      <div className="ml-5">
        <Paragraph value={A}></Paragraph>
        <Paragraph value={B}></Paragraph>
      </div>
      <nav className="text-center">
        <Link to="/patient/puipatient" className="btn btn-primary mr-5">
          Yes
        </Link>
        <Link to="/patient/notpuiorpumpatient" className="btn btn-primary ml-5">
          No
        </Link>
      </nav>
    </div>
  );
};

export default AC;
