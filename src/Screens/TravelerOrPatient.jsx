import React from "react";
import { Link } from "react-router-dom";

const TravelerOrPatient = () => {
  return (
    <div className="text-center">
      <nav>
        <Link to="/traveler/travelhistory" className="btn btn-primary mr-5">
          Traveler
        </Link>
        <Link
          to="/patient/acuterespiratoryillness"
          className="btn btn-primary ml-5"
        >
          Patient
        </Link>
      </nav>
    </div>
  );
};

export default TravelerOrPatient;
