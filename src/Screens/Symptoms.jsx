import React from "react";
import { Link } from "react-router-dom";
import Paragraph from "../Components/Paragraph";

const Symptoms = () => {
  const msg =
    "Fever(>= 38.0 Degrees) AND/OR Respiratory illness(cough and/or colds)";
  return (
    <div className="text-center">
      <h3 className="text-center">Symptoms</h3>
      <Paragraph value={msg}></Paragraph>
      <nav className="text-center">
        <Link to="/traveler/pui" className="btn btn-primary mr-5">
          Yes
        </Link>
        <Link to="/traveler/pum" className="btn btn-primary ml-5">
          No
        </Link>
      </nav>
    </div>
  );
};

export default Symptoms;
