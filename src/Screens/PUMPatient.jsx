import React from "react";
import Paragraph from "../Components/Paragraph";

const PUMPatient = () => {
  const msg =
    "ASYMPTOMATIC patients with appropriate EXPOSURE history should undergo home quarantine for 14 days to monitor for the development of symptoms: ";
  return (
    <div className="ml-5">
      <h3 className="text-center">Person Under Monitoring (PUM)</h3>
      <div>
        <Paragraph value={msg}></Paragraph>
        <ul>
          <li>Inform RESU</li>
          <li>Fill out CIF</li>
          <li>NO NEED for testing</li>
        </ul>
      </div>
    </div>
  );
};

export default PUMPatient;
