import React from "react";
import Paragraph from "../Components/Paragraph";

const PUM = () => {
  const msg = "Home Quarantine";
  return (
    <div className="text-center">
      <h3 className="text-center">Person Under Monitoring (PUM)</h3>
      <Paragraph value={msg}></Paragraph>
    </div>
  );
};

export default PUM;
