import React from "react";
import Paragraph from "../Components/Paragraph";

const PUIPatient = () => {
  const msg = "Verify and coordinate with RESU";
  const msg2 = "Complete the case investigation form(CIF)";
  const msg3 = "ADMIT to designated COVID-19 isolation area";
  return (
    <div className="ml-5">
      <h3 className="text-center m-5">Patients Under Investigation (PUI)</h3>
      <div id="pui">
        <Paragraph value={msg}></Paragraph>
        <Paragraph value={msg2}></Paragraph>
        <Paragraph value={msg3}></Paragraph>
      </div>
    </div>
  );
};

export default PUIPatient;
