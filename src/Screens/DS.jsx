import React from "react";
import Paragraph from "../Components/Paragraph";
import { Link } from "react-router-dom";

const DS = () => {
  const A = "Did the symptoms occur within 14 days of exposure?";

  return (
    <div>
      <div className="ml-5">
        <Paragraph value={A}></Paragraph>
      </div>
      <nav className="text-center">
        <Link to="/patient/puipatient" className="btn btn-primary mr-5">
          Yes
        </Link>
        <Link to="/patient/notpuiorpumpatient" className="btn btn-primary ml-5">
          No
        </Link>
      </nav>
    </div>
  );
};

export default DS;
