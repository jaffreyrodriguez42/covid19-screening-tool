import React from "react";
import Paragraph from "../Components/Paragraph";
import { Link } from "react-router-dom";

const TC = () => {
  const A =
    "A. Travel to or residence in a country/area reporting local transmission of COVID-19 OR.";
  const B =
    "B. CLOSE CONTACT with a confirmed COVID-19 case (any of the following). ";

  return (
    <div>
      <div className="ml-5">
        <Paragraph value={A}></Paragraph>
        <Paragraph value={B}></Paragraph>
        <ul>
          <li>
            Providing direct care without proper PPE to confirmed COVID-19
            patient.
          </li>
          <li>
            Staying in the same close environment (incl. workplace, classroom,
            household, gatherings)
          </li>
          <li>
            Traveling together in close proximity (1 meter or 3 feet) in any
            kind of conveyance.
          </li>
        </ul>
      </div>
      <nav className="text-center">
        <Link to="/patient/ds" className="btn btn-primary mr-5">
          Yes
        </Link>
        <Link to="/patient/ac" className="btn btn-primary ml-5">
          No
        </Link>
      </nav>
    </div>
  );
};

export default TC;
