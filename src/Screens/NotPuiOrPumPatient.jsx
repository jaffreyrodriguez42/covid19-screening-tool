import React from "react";
import Paragraph from "../Components/Paragraph";

const NotPuiOrPumPatient = () => {
  const msg =
    "Refer to ER or other clinic for appropriate work-up and management.";
  return (
    <div className="text-center">
      <h3 className="text-center">Not PUI Or PUM</h3>
      <Paragraph value={msg}></Paragraph>
    </div>
  );
};

export default NotPuiOrPumPatient;
