import React from "react";
import { Link } from "react-router-dom";
import Paragraph from "../Components/Paragraph";

const TravelHistory = (props) => {
  console.log(props.travelHistory);
  return (
    <div className="text-center">
      <h3 className="text-center">Travel History</h3>
      <Paragraph value={props.travelHistory}></Paragraph>
      <nav>
        <Link to="/traveler/symptoms" className="btn btn-primary mr-5">
          Yes
        </Link>
        <Link to="/traveler/historyofexposure" className="btn btn-primary ml-5">
          No
        </Link>
      </nav>
    </div>
  );
};

export default TravelHistory;
