import React from "react";
import Paragraph from "../Components/Paragraph";

const NotPuiOrPum = () => {
  const msg = "Manage appropriately.";
  return (
    <div className="text-center">
      <h3 className="text-center">Not PUI Or PUM</h3>
      <Paragraph value={msg}></Paragraph>
    </div>
  );
};

export default NotPuiOrPum;
