import React from "react";
import Paragraph from "../Components/Paragraph";

const PUI = () => {
  const msg = "If with mild manifestations, refer to L2/L3 hospital";
  const msg2 = "If with severe manifestations, refer to Referral Hospitals";
  return (
    <div className="text-center">
      <h3 className="text-center">Patients Under Investigation (PUI)</h3>
      <Paragraph value={msg}></Paragraph>
      <Paragraph value={msg2}></Paragraph>
    </div>
  );
};

export default PUI;
