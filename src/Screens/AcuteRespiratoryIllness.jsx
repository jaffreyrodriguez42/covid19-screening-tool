import React from "react";
import Paragraph from "../Components/Paragraph";
import { Link } from "react-router-dom";

const AcuteRespiratoryIllness = () => {
  const A = "A. Fever(>= 38 Degrees OR).";
  const B =
    "B. Cough OR shortness of breath OR other respiratory symptoms OR. ";
  const C = "C. Diarrhea. ";

  return (
    <div>
      <h3 className="text-center">Acute Respiratory Illness</h3>
      <div className="ml-5">
        <Paragraph value={A}></Paragraph>
        <Paragraph value={B}></Paragraph>
        <Paragraph value={C}></Paragraph>
      </div>
      <nav className="text-center">
        <Link to="/patient/tc" className="btn btn-primary mr-5">
          Yes
        </Link>
        <Link to="/patient/pumpatient" className="btn btn-primary ml-5">
          No
        </Link>
      </nav>
    </div>
  );
};

export default AcuteRespiratoryIllness;
