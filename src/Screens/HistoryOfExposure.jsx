import React from "react";
import Paragraph from "../Components/Paragraph";
import { Link } from "react-router-dom";

const HistoryOfExposure = () => {
  const A = "A. Providing direct care for COVID-19 patient.";
  const B =
    "B. Working together or staying in the same close environment of a COVID-19. ";
  const C =
    "C. Traveling together with COVID-19 patient in any kind of conveyance. ";
  const D =
    "D. Living in the same household as a COVID-19 patient within a 14-day period. ";

  return (
    <div>
      <h3 className="text-center">History Of Exposure</h3>
      <div className="ml-5">
        <Paragraph value={A}></Paragraph>
        <Paragraph value={B}></Paragraph>
        <Paragraph value={C}></Paragraph>
        <Paragraph value={D}></Paragraph>
      </div>
      <nav className="text-center">
        <Link to="/traveler/symptoms" className="btn btn-primary mr-5">
          Yes
        </Link>
        <Link to="/traveler/notpuiorpum" className="btn btn-primary ml-5">
          No
        </Link>
      </nav>
    </div>
  );
};

export default HistoryOfExposure;
