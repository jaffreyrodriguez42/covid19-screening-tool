import React from "react";

const Paragraph = (props) => {
  return <p>{props.value}</p>;
};

export default Paragraph;
